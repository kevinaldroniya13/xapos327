drop table category

create table category(
	id bigint generated always as identity primary key not null,
	category_initial varchar(10) not null,
	category_name varchar(50) not null,
	create_by varchar(50),
	create_date timestamp not null,
	is_active boolean not null,
	modify_by varchar(50) null,
	modify_date timestamp null
)


INSERT INTO category
(category_initial,category_name,create_by,create_date,is_active,modify_by,modify_date)
VALUES
('Drink','Menu Of Drink','Admin 1',now(),true,null,null)


SELECT * FROM category


CREATE TABLE variarnt
(
	id bigint generated always as identity primary key not null,
	category_id bigint not null,
	variant_initial varchar(50) not null,
	variant_name varchar(50) not null,
	is_active boolean not null,
	create_by varchar(50) not null,
	create_date timestamp not null,
	modify_by varchar(50) null,
	modify_date timestamp null
)

SELECT * FROM variant

INSERT INTO variant
(category_id,variant_initial,variant_name,is_active,create_by,create_date,modify_by,modify_date)
VALUES
(1,'Es','Es Teh',true,'Admin 1',now(),null,null)


CREATE TABLE products
(
	id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY NOT NULL,
	variant_id bigint NOT NULL,
	product_initial varchar(50) NOT NULL,
	product_name varchar(50) NOT NULL,
	description varchar(255) NOT NULL,
	is_active boolean NOT NULL,
	price double precision NOT NULL,
	stock double precision NOT NULL,
	create_by varchar(50) NOT NULL,
	create_date timestamp NOT NULL,
	modify_by varchar(50) NULL,
	modify_date timestamp NULL
)

DROP TABLE order_header

CREATE TABLE order_header
(
	id bigint generated always as identity primary key not null,
	amount double precision not null,
	create_by varchar(50) not null,
	create_date timestamp not null,
	is_active boolean not null,
	reference varchar(255)not null,
	modify_by varchar(50) null,
	modify_date timestamp null
	
)

CREATE TABLE order_details
(
	id bigint generated always as identity primary key not null,
	header_id bigint not null,
	product_id bigint not null,
	quantity double precision not null,
	price double precision not null,
	is_active boolean not null,
	create_by varchar(50) not null,
	create_date timestamp not null,
	modify_by varchar(50) null,
	modify_date timestamp null
)

DROP TABLE variarnt

ALTER TABLE order_detail
add foreign key(header_id) references order_header(id)

ALTER TABLE order_detail
add foreign key(product_id) references products(id)

select * from order_header
select * from order_detail

ALTER TABLE category
ADD COLUMN sphoto varchar(255)

select * from category

CREATE TABLE role
(
	id bigint generated always as identity primary key not null,
	role_name varchar(80) null,
	is_delete Boolean not null,
	created_by varchar(50) not null,
	created_date timestamp not null,
	updated_by varchar(50) null,
	updated_date timestamp null
)


select * from role where is_delete = false