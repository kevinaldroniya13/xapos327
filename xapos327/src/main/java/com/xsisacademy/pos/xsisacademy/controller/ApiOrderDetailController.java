package com.xsisacademy.pos.xsisacademy.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.pos.xsisacademy.model.OrderDetail;
import com.xsisacademy.pos.xsisacademy.repository.OrderDetailRepository;

@RestController
@RequestMapping("/api/transaction/")
public class ApiOrderDetailController {

	@Autowired
	private OrderDetailRepository orderDetailRepository;

	@PostMapping("orderdetail/add")
	public ResponseEntity<Object> saveOrderDetail(@RequestBody OrderDetail orderDetail) {
		orderDetail.isActive = true;
		orderDetail.createBy = "Admin 1";
		orderDetail.createDate = new Date();

		OrderDetail orderDetailData = this.orderDetailRepository.save(orderDetail);
		if (orderDetailData.equals(orderDetail)) {
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

	}

	@GetMapping("orderdetailbyid/{id}")
	public ResponseEntity<Object> getOrderDetailById(@PathVariable("id") Long id) {
		try {
			Optional<OrderDetail> orderDetail = this.orderDetailRepository.findById(id);
			return new ResponseEntity<>(orderDetail, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("orderdetailbyheaderid/{headerId}")
	public ResponseEntity<List<OrderDetail>> getOrderByHeaderId(@PathVariable("headerId") Long id) {
		try {
			List<OrderDetail> orderDetails = this.orderDetailRepository.findOrderByHeaderId(id);
			return new ResponseEntity<>(orderDetails, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("orderdetailchkeckout/{id}")
	public ResponseEntity<Object> orderDetailChekOut(@PathVariable("id") Long id) {
		try {
			OrderDetail orderDetailData = this.orderDetailRepository.findById(id).orElse(null);
			orderDetailData.modifyBy = "Admin 1";
			orderDetailData.modifyDate = new Date();
			this.orderDetailRepository.save(orderDetailData);
			return new ResponseEntity<>("Checkout Od_id Success", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("orderdetail/update/{id}")
	public ResponseEntity<Object> updateOD(@PathVariable("id") Long id, @RequestBody OrderDetail orderDetail) {
		Optional<OrderDetail> orderDetailData = this.orderDetailRepository.findById(id);
		if (orderDetailData.isPresent()) {
			orderDetail.id = id;
			orderDetail.createBy = orderDetailData.get().createBy;
			orderDetail.createDate = orderDetailData.get().createDate;
			orderDetail.headerId = orderDetailData.get().headerId;
			orderDetail.productId = orderDetailData.get().productId;
			orderDetail.price = orderDetailData.get().price;
			orderDetail.isActive = orderDetailData.get().isActive;
			this.orderDetailRepository.save(orderDetail);
			return new ResponseEntity<>("Update Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("deletepermanent/{id}")
	public ResponseEntity<Object> deleteOD(@PathVariable("id")Long id){
		try {
			OrderDetail orderDetailData = this.orderDetailRepository.findById(id).orElse(null);
			orderDetailData.modifyBy = "Admin 1";
			orderDetailData.modifyDate = new Date();
			orderDetailData.isActive = false;
			this.orderDetailRepository.save(orderDetailData);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	/*
	@DeleteMapping("deletepermanent/{id}")
	public ResponseEntity<Object> deleteOD(@PathVariable Long id){
		try {
			OrderDetail orderDetailData = this.orderDetailRepository.findById(id).orElse(null);
			this.orderDetailRepository.delete(orderDetailData);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	*/

}
