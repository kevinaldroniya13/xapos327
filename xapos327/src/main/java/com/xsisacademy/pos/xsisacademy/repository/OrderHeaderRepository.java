package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.OrderHeader;

public interface OrderHeaderRepository extends JpaRepository<OrderHeader, Long>{
	
	@Query("SELECT MAX(o.id) FROM OrderHeader o")//JAVA CLASS
	public Long findByMaxId();
	
	//@Query(value="SELECT * FROM order_header WHERE is_active = false", nativeQuery=true)
	//List<OrderHeader> findAllOrderHeaderChekout();

	@Query(value="SELECT * FROM order_header WHERE amount != 0 ORDER BY modify_date DESC", nativeQuery=true)
	List<OrderHeader> findAllOrderHeaderChekout();
	
}
