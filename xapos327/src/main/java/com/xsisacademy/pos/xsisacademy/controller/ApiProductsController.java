package com.xsisacademy.pos.xsisacademy.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.pos.xsisacademy.model.Products;
import com.xsisacademy.pos.xsisacademy.model.Variant;
import com.xsisacademy.pos.xsisacademy.repository.ProductsRepository;
import com.xsisacademy.pos.xsisacademy.repository.VariantRepository;

@RestController
@RequestMapping("/api/")
public class ApiProductsController {
	@Autowired
	private ProductsRepository productRepository;

	@Autowired
	private VariantRepository variantRepository;

	@GetMapping("products")
	public ResponseEntity<List<Products>> getAllProducts() {
		try {
			List<Products> listProducts = this.productRepository.findByIsActive();
			return new ResponseEntity<>(listProducts, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("products/variantByCategoryId/{categoryId}")
	public ResponseEntity<List<Variant>> getVariantByCategoryId(@PathVariable("categoryId") Long categoryId) {
		try {
			List<Variant> listVariant = this.variantRepository.findByCategoryId(categoryId);
			return new ResponseEntity<>(listVariant, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("products/add")
	public ResponseEntity<Object> saveProduct(@RequestBody Products products) {
		products.createBy = "Admin 1";
		products.createDate = new Date();

		Products productsData = this.productRepository.save(products);

		if (productsData.equals(products)) {
			return new ResponseEntity<>("Save Data Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Data Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("products/{id}")
	public ResponseEntity<Object> getProductById(@PathVariable("id") Long id) {
		try {
			Optional<Products> products = this.productRepository.findById(id);
			return new ResponseEntity<>(products, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("products/edit/{id}")
	public ResponseEntity<Object> updateProduct(@PathVariable("id") Long id, @RequestBody Products products) {
		Optional<Products> productsData = this.productRepository.findById(id);
		if (productsData.isPresent()) {
			products.id = id;
			products.modifyBy = "Admin 1";
			products.modifyDate = new Date();
			products.createBy = productsData.get().createBy;
			products.createDate = productsData.get().createDate;

			this.productRepository.save(products);
			return new ResponseEntity<>("Update Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("products/delete/{id}")
	public ResponseEntity<Object> deleteProduct(@PathVariable("id") Long id, @RequestBody Products products) {
		Optional<Products> productsData = this.productRepository.findById(id);
		if (productsData.isPresent()) {
			products.id = id;
			products.modifyBy = "Admin 1";
			products.modifyDate = new Date();
			products.isActive = false;
			products.createBy = productsData.get().createBy;
			products.createDate = productsData.get().createDate;
			products.productInitial = productsData.get().productInitial;
			products.productName = productsData.get().productName;
			products.price = productsData.get().price;
			products.stock = productsData.get().stock;
			products.description = productsData.get().description;
			this.productRepository.save(products);
			return new ResponseEntity<>("Delete Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("products/checkout/{productId}/{stockLeft}")
	public ResponseEntity<Object> productCheckout(@PathVariable("productId") Long id,
			@PathVariable("stockLeft") double stock) {
		try {
			Products productsData = this.productRepository.findById(id).orElse(null);
			productsData.modifyBy = "Admin 1";
			productsData.modifyDate = new Date();
			productsData.stock = stock;
			this.productRepository.save(productsData);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

}
