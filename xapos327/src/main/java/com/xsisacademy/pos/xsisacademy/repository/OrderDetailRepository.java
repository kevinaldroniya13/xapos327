package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.OrderDetail;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long>{
	@Query(value="SELECT * FROM order_detail WHERE header_id = ?1 AND is_active=true", nativeQuery=true)
	List<OrderDetail> findOrderByHeaderId(Long id);
}
