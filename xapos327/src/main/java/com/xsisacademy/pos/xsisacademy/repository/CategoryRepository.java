package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

	@Query(value = "SELECT * FROM category WHERE is_active = true ORDER BY category_name", nativeQuery = true)
	// @Query(value="SELECT c FROM Category c WHERE c.isActive = true") //Java Class
	List<Category> findByCategories();

	// Paging//
	@Query(value = "SELECT * FROM category WHERE LOWER(category_name) LIKE LOWER(CONCAT('%',?1,'%')) AND is_active =?2 ORDER BY category_name ASC", nativeQuery = true)
	Page<Category> findByIsActive(String keyword, Boolean isActive, Pageable page);

	@Query(value = "SELECT * FROM category WHERE LOWER(category_name) LIKE LOWER(CONCAT('%',?1,'%')) AND is_active =?2 ORDER BY category_name DESC", nativeQuery = true)
	Page<Category> findByIsActiveDESC(String keyword, Boolean isActive, Pageable page);

	@Query(value = "SELECT * FROM category WHERE is_active = true AND category_name =?1 LIMIT 1", nativeQuery = true)
	Category findByIdName(String categoryName);

	@Query(value = "SELECT * FROM category WHERE is_active = true AND category_name =?1 AND id != ?2 LIMIT 1", nativeQuery = true)
	Category findByIdNameForEdit(String categoryName, Long id);

}
