package com.xsisacademy.pos.xsisacademy.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.pos.xsisacademy.model.OrderDetail;
import com.xsisacademy.pos.xsisacademy.model.OrderHeader;
import com.xsisacademy.pos.xsisacademy.model.Products;
import com.xsisacademy.pos.xsisacademy.repository.OrderDetailRepository;
import com.xsisacademy.pos.xsisacademy.repository.OrderHeaderRepository;
import com.xsisacademy.pos.xsisacademy.repository.ProductsRepository;

@RestController
@RequestMapping("/api/transaction/")
public class ApiOrderCotroller {

	@Autowired
	private OrderHeaderRepository orderHeaderRepository;

	@Autowired
	private OrderDetailRepository orderDetailRepository;

	@Autowired
	private ProductsRepository productsRepository;

	@PostMapping("orderheader/create")
	public ResponseEntity<Object> createReference() {
		OrderHeader orderHeader = new OrderHeader();
		String timeDec = String.valueOf(System.currentTimeMillis());

		orderHeader.reference = timeDec;
		orderHeader.amount = 0;
		orderHeader.isActive = true;
		orderHeader.createBy = "Admin 1";
		orderHeader.createDate = new Date();

		OrderHeader orderHeaderData = this.orderHeaderRepository.save(orderHeader);

		if (orderHeaderData.equals(orderHeader)) {
			return new ResponseEntity<>("Create Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Create Failed", HttpStatus.NO_CONTENT);
		}

	}

	@GetMapping("maxorderheaderid")
	public ResponseEntity<Long> getMaxOrderHeader() {
		try {
			Long maxId = this.orderHeaderRepository.findByMaxId();
			return new ResponseEntity<Long>(maxId, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}

	/*
	 * @PutMapping("orderheadercheckout/{headerId}/{totalAmount}") public
	 * ResponseEntity<Object> orderHeaderCheckout(@PathVariable("headerId")Long
	 * headerId, @PathVariable("totalAmount")double totalAmount){ try { OrderHeader
	 * orderHeaderData = this.orderHeaderRepository.findById(headerId).orElse(null);
	 * orderHeaderData.amount = totalAmount; orderHeaderData.modifyBy = "Admin 1";
	 * orderHeaderData.modifyDate = new Date();
	 * 
	 * 
	 * 
	 * this.orderHeaderRepository.save(orderHeaderData); return new
	 * ResponseEntity<>("Checkout Success", HttpStatus.OK); } catch (Exception e) {
	 * return new ResponseEntity<>(HttpStatus.NO_CONTENT); } }
	 */

	@PutMapping("orderheadercheckout/{headerId}/{totalAmount}")
	public ResponseEntity<Object> orderHeaderCheckout(@PathVariable("headerId") Long headerId,
			@PathVariable("totalAmount") double totalAmount) {
		try {
			OrderHeader orderHeaderData = this.orderHeaderRepository.findById(headerId).orElse(null);
			orderHeaderData.amount = totalAmount;
			orderHeaderData.modifyBy = "Admin 1";
			orderHeaderData.modifyDate = new Date();

			List<OrderDetail> orderDetail = this.orderDetailRepository.findOrderByHeaderId(headerId);

			for (OrderDetail item : orderDetail) {
				Products products = item.products;
				products.stock -= item.quantity;

				this.productsRepository.save(products);
			}
			this.orderHeaderRepository.save(orderHeaderData);
			return new ResponseEntity<>("Checkout Success", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	/*
	 * @PutMapping("orderheadercheckout/{headerId}") public ResponseEntity<Object>
	 * orderHeaderCheckout(@PathVariable("headerId") Long id,
	 * 
	 * @RequestBody OrderHeader orderHeader) { Optional<OrderHeader> orderHeaderData
	 * = this.orderHeaderRepository.findById(id); if (orderHeaderData.isPresent()) {
	 * orderHeader.id = id; orderHeader.modifyBy = "Admin 1"; orderHeader.modifyDate
	 * = new Date(); orderHeader.isActive = false;
	 * 
	 * orderHeader.createBy = orderHeaderData.get().createBy; orderHeader.createDate
	 * = orderHeaderData.get().createDate; orderHeader.reference =
	 * orderHeaderData.get().reference;
	 * 
	 * this.orderHeaderRepository.save(orderHeader); return new
	 * ResponseEntity<>("Checkout Success", HttpStatus.OK); } else { return
	 * ResponseEntity.notFound().build(); } }
	 */
	@GetMapping("getOrderHeaderCheckout")
	public ResponseEntity<List<OrderHeader>> getAllOrderHeaderCheckout() {
		try {
			List<OrderHeader> listOrderHeader = this.orderHeaderRepository.findAllOrderHeaderChekout();
			return new ResponseEntity<>(listOrderHeader, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

}
