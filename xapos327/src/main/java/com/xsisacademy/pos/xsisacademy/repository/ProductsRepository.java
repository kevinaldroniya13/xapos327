package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Products;

public interface ProductsRepository extends JpaRepository<Products, Long>{
	@Query(value="SELECT * FROM products WHERE is_active = true ORDER BY product_name", nativeQuery=true)
	List<Products> findByIsActive();
	
}
