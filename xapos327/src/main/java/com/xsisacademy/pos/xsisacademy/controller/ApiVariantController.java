package com.xsisacademy.pos.xsisacademy.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.pos.xsisacademy.model.Variant;
import com.xsisacademy.pos.xsisacademy.repository.VariantRepository;

@RestController
@RequestMapping("/apiVariant/")
public class ApiVariantController {

	@Autowired
	VariantRepository variantRepository;

	@GetMapping("getVariant")
	public ResponseEntity<List<Variant>> getAllVariant() {
		try {
			List<Variant> listVariant = this.variantRepository.findAllVariants();

			return new ResponseEntity<>(listVariant, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("getVariant/add")
	public ResponseEntity<Object> saveVariant(@RequestBody Variant variant) {
		variant.createBy = "Admin 1";
		variant.createDate = new Date();
		// cara lain
		// variant.setCreateBy("Admin 1");
		// variant.setCreateDate(new Date());

		Variant variantData = this.variantRepository.save(variant);

		if (variantData.equals(variant)) {
			return new ResponseEntity<>("Save Data Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Data Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("getVariant/{id}")
	public ResponseEntity<Object> getVariantById(@PathVariable("id") Long id) {
		try {
			Optional<Variant> variant = this.variantRepository.findById(id);
			return new ResponseEntity<>(variant, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("getVariant/edit/{id}")
	public ResponseEntity<Object> updateVariant(@PathVariable("id") Long id, @RequestBody Variant variant) {
		Optional<Variant> variantData = this.variantRepository.findById(id);
		if (variantData.isPresent()) {
			variant.id = id;
			variant.modifyBy = "Admin 1";
			variant.modifyDate = new Date();
			variant.createBy = variantData.get().createBy;
			variant.createDate = variantData.get().createDate;

			this.variantRepository.save(variant);
			return new ResponseEntity<>("Update Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	/*
	 * @PutMapping("getVariant/delete/{id}") public ResponseEntity<Object>
	 * deleteVariant(@PathVariable("id") Long id, @RequestBody Variant variant) {
	 * Optional<Variant> variantData = this.variantRepository.findById(id); if
	 * (variantData.isPresent()) { variant.id = id; variant.isActive = false;
	 * variant.modifyBy = "Admin 1"; variant.modifyDate = new Date();
	 * variant.categoryId = variantData.get().categoryId; variant.variantInitial =
	 * variantData.get().variantInitial; variant.variantName =
	 * variantData.get().variantName; variant.createBy = variantData.get().createBy;
	 * variant.createDate = variantData.get().createDate;
	 * 
	 * this.variantRepository.save(variant); return new
	 * ResponseEntity<>("Delete Success", HttpStatus.OK); } else { return
	 * ResponseEntity.notFound().build(); } }
	 */

	@PutMapping("getVariant/delete/{id}")
	public ResponseEntity<Object> deleteVariant(@PathVariable("id") Long id) {
		Optional<Variant> variantData = this.variantRepository.findById(id);
		if (variantData.isPresent()) {
			Variant variant = new Variant();
			variant.id = id;
			variant.isActive = false;
			variant.modifyBy = "Admin 1";
			variant.modifyDate = new Date();
			variant.categoryId = variantData.get().categoryId;
			variant.variantInitial = variantData.get().variantInitial;
			variant.variantName = variantData.get().variantName;
			variant.createBy = variantData.get().createBy;
			variant.createDate = variantData.get().createDate;

			this.variantRepository.save(variant);
			return new ResponseEntity<>("Delete Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	// Paging
	@GetMapping("getVariant/paging")
	public ResponseEntity<Map<String, Object>> getAllVariantPages(@RequestParam(defaultValue = "0") int currentPage,
			@RequestParam(defaultValue = "5") int size, @RequestParam("keyword") String keyword,
			@RequestParam("sortType") String sortType) {
		try {
			List<Variant> variant = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(currentPage, size);

			Page<Variant> pages;
			if (sortType.equals("ASC")) {
				pages = this.variantRepository.findByIsActive(keyword, true, pagingSort);
			} else {
				pages = this.variantRepository.findByIsActiveDESC(keyword, true, pagingSort);
			}
			variant = pages.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("pages", pages.getNumber());
			response.put("total", pages.getTotalElements());
			response.put("total_pages", pages.getTotalPages());
			response.put("data", variant);

			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
