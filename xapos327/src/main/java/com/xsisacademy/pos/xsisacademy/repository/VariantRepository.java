package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Variant;

public interface VariantRepository extends JpaRepository<Variant, Long> {

	@Query(value = "SELECT * FROM variant WHERE is_active = true ORDER BY variant_name", nativeQuery = true)
	List<Variant> findAllVariants();

	// List<Variant> findByisActive(Boolean isActive);

	// menggunakan 2 parameter
	// @Query(value="SELECT V FROM variant V WHERE is_active= ?1 AND variantInitial=
	// ?2 ", nativeQuery=true)
	// List<Varaint> findByIsActiveAndInitial(Boolean isActive, String
	// productInitial);
	
	@Query(value = "SELECT * FROM variant WHERE is_active = true AND category_id = ?1 ORDER BY variant_name", nativeQuery = true)
	List<Variant> findByCategoryId(Long categoryId);

	@Query(value = "SELECT * FROM variant WHERE LOWER(variant_name) LIKE LOWER(CONCAT('%',?1,'%')) AND is_active =?2 ORDER BY variant_name ASC", nativeQuery = true)
	Page<Variant> findByIsActive(String keyword, Boolean isActive, Pageable page);

	@Query(value = "SELECT * FROM variant WHERE LOWER(variant_name) LIKE LOWER(CONCAT('%',?1,'%')) AND is_active =?2 ORDER BY variant_name DESC", nativeQuery = true)
	Page<Variant> findByIsActiveDESC(String keyword, Boolean isActive, Pageable page);
}