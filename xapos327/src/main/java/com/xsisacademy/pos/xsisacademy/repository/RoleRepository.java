package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
	
	@Query(value="SELECT * FROM role ORDER BY role_name ASC WHERE is_delete=false", nativeQuery=true)
	List<Role> findAllRole();
	
	@Query(value="SELECT * FROM role WHERE LOWER(role_name) LIKE LOWER(CONCAT('%',?1,'%')) AND is_delete=false ORDER BY role_name ASC", nativeQuery=true)
	Page<Role> findAllRoleASC(String keyword, Pageable page);
	
	@Query(value="SELECT * FROM role WHERE LOWER(role_name) LIKE LOWER(CONCAT('%',?1,'%')) AND is_delete=false ORDER BY role_name DESC", nativeQuery=true)
	Page<Role> findAllRoleDESC(String keyword, Pageable page);

}
